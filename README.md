# Highlights

## So far, you will be the only other person besides myself that knows any of this...

# About you
You have been brought up by the Sund'Arin (see below), and you've had a sense of
appreciation and loyalty to them for taking care of you all these years.  Very
recently, you have been promoted to Deputy (see ranks below) under Faroht
Darmita Zefree, an older halfling woman of the Sund'Arin Eldership.  Presently,
you and Darmita have been tasked with locating someone who seems to have jumped
multiple portals and ended up in the Estrain Territory of Udrais.  This place is
a desert, full of nothing but sand dunes and blazing heat.  The jumper was
reported to be near the sandstone city of Avolis (av-oh-leez), found by local
members of the Sund'Arin.  Darmita has been commanded to either recruit this
jumper into the Sund'Arin, or kill them.  However, this command sounds odd to
you, because generally no option is given as entering a forbidden gate means
death.

You have never seen any of the Forbidden Gates yourself and have only ever met a
handful of people who have been marked by Anwa. (Anwa explained below)

# Magic
Magic has only been widely known in this world for 342 years.  Until
that point, only a handful of remote tribes and the Sund'Arin knew magic
existed.  It naturally occurs in deep in the ground, and forms in pockets or
veins, much like metallic ores or minerals.  In its pure form, its state is like
a radiant gel substance, with varying colors which seem to differ by the
geographic location it is found.  However the substance is highly volatile, and
when attempting to mine or harvest the material, expert care must be taken or it will
explode with nuclear-esk power.

# The Sund'Arin (Soond-areen)
A secret organization (you have been a member since you were young) sworn to
protect the existence of magic gates in the world.  A "gate" is portal or rift,
generated when a vein of magic naturally comes in contact with the gases in the
atmosphere.  The occurrence is extrememly rare, and as of the present day, only
10 are known to exist in the world.  To become a member, a small ceremony called
the "Oath of the Coi Naxa" (Life Binding Oath) where initiates drink from a cup
are then magically marked on their left wrist by a faintly glowing tattoo of a
crescent moon over a gate.
During ceremony, an elder of the Sund'Arin may wear a red sash over their
shoulder across their torso, but there are no other distinguishing features of
the organization.

### Ranks of the Sund'Arin
```
Members:
	Fenna Tiris (fenna tee-rees) -
		Anyone who has taken the Oath of the Coi Naxa to become a member.  Most of
		the time, a member title is shorted to "Tiris" (or "Keeper" in common).
	Fenna Lusion (fenna loo-shun)-
		An elite member of the Sund'Arin gifted with pure magic directly from one of
		the gates - a rite typically reserved for Farohts and Elders called Anwa-
		When addressed by title, they are called "Lusion" (or "Wanderer" in common).
		There is a ranking within Fenna Lusion, called the "Lusion Order" - which
		corresponds to how many giftings have occurred.  A Lusion with a single
		gifting is called a "1st Order Lusion", a second gifting, a "2nd Order
		Lusion" and so on.  Any member of the Sund'Arin can identify a Lusion
		because they can see a faint aura around them in the color of magic they
		were gifted from.
	Deputy -
		A general assistant to a Faroht
	Watcher - a member whos sole responsibility is to keep watch on Ripples caused
		by the magic Gates.  A "Ripple" is a commotion in the fabric of magic that
		now covers the world, that gives the Watcher the ability to track when a
		Gate has been accessed, and to know where a Jumper has ended up after
		passing through a Gate.
Elders:
	Faroht (like it sounds) -
		Direct servants of the regent - tasked to carry out the commands of the regent
		and the Ecclaisson Conclave.
	Regent -
		Commander of a region - tasked with oversight and protection of the
		gate where they are posted.
	Ecclais (ehk-lay-z)
		One of ten - the highest rank of the Sund'Arin - member of the High Council
		or the Ecclaisson (ehk-lay-zawn) Conclave of the Sund'Arin.
```

# The Destruction (known as Tura Lothen to the Sund'Arin)
An event that occurred 342 years ago, in the Xuscos Territory of the continent
of Udrias.  Two kingdoms, The Mourean and the Suivell were at war over a portion
of land the Mourean staked claim to.  The land was occupied by a tribe called
the Praryn who, in conjunction with the Sund'Arin, was protecting the first
known gate in the world.  The leader of the Mourean, Queen Hethis, discovered
the existence of the gate after her kingdom took possession of the land.
Recognizing its power, she sought to use the magic to help her push back the
Suivell, but she didn't understand its volatility.  Attempting to mine the magic
vein near the 1st Gate, the vein ruptured and the gate collapsed, causing a
compound reaction that decimated the entire region and destroyed both kingoms in
an instant.  For 3 days following the event, massive earthquakes reshaped the
landscape in this quarter of the world.

When the vein of magic ruptured, it released its energy into the atmosphere,
where it soon covered the world.  It is not known by the world (or the party)
the details of the event - only that there was great devastation, and then magic
was in the world.

# Forbidden Gates
At this time, there have only been 11 gates known by the Sund'Arin, named in order
they were discovered. The 1st Gate was lost in The Destruction, leaving now only
10 remaining.  Many of the gates are portals that only bend space within the
confines of the world, but some are rifts to other realms.  It is forbidden for
any person, Sund'Arin or otherwise to go near a gate.  The Sund'Arin are only
charged with protecting the secret locations of the gates at all costs.

As the gates are formed at pure magical sources, anyone who has been in close
proximity to one for long is marked by a permanent infusing of magic called,
Anwa.  Typically this gifting is a rite reserved for the Farohts and Elders of the
Sund'Arin.  When the infusion has taken place, anyone who has taken the Oath of
the Coi Naxa can see a sort of faint aura around the person in the color of
magic they were infused by, but non-members cannot see it.

# Ideas from our discussions:
```
Quick idea just fire background. When was the last explosion of improper mining
of magic?  As a monk I could have been in one of those and the power was latent
until now. That's how I followed the path of the four elements???
```
```
So my mother and father raised me till I was 4 years old at a dojo for martial
arts. Our village was massacred except me by the Cerin Empire. Rumor had it that
some deposits were found under the village and they wanted to excavate it.

Upon capturing me and doing test and using me for labor about two years later I
escaped and ran. It was only 2 more years before they found me and if it weren't
for the Sund'Arin who got to me first I would have ended up who knows where.

I trained with them for the following 20 years until now. Now my body is
experiencing the experiments side effects. And is somehow allowing me to access
elemental magic. It is still new to me as it only surfaced one year ago.

I am now 28 and have seen and been through many things, but I will control this
power and make it my own!
```
```
Village of Eel'amonts
```
```
I thought about the past and Genasi.

There's a few passive skills they have but I would say I don't know them since I
was never taught. And my lineage is that of human male and a  genie female.

Normally genies don't have anything to do with their offspring. So she wasn't in
my life (not because she didn't want to, something else prevented)

So naturally he developed an affinity to air but thought he was just swift.

Since he didn't get to train from his dojo at an age to understand elements, he
only learned the physical teachings before the village was murdered. There might
be other survivors?

After being caught and subjected to experimentation via the veins of the gates,
his latent powers began surfacing over the years.

He has not been around a gate itself and hasn't been in contact with anything
since.

Through the Sund'Arin he continued training from different teachers around the
area. And learned about the history behind the path of four elements.

With this newly found power he hopes to control he is still scared from his
captive times.

He only remembers his fathers eyes, silhouette and name.

Tall and fit, looks like short hair.  A piercing yellow/orange look.  His
Fathers name was Yem, Short for Yem'Halku.

And so he doesn't think back on his lineage as much as revenge to the empire and
looking ahead towards his future and the savors of the Sund'Arin.

The Village was home to Other Air Genasi and humans and elf's mainly.

It was a popular dojo they ran. People from all over would come learn. The
genasi were trying to protect the sacred ground but without knowing what was
actually under the village. (And then the fire nation attacked) lol then the
empire case in and did all that.

```
